
# Service 152 - Aéroport de Strasbourg

## Informations générales

| | |
| --------- | -----|  
| ID : | 152 |  
| Nom : | Aéroport de Strasbourg | 
| Site : | https://www.strasbourg.aeroport.fr/ |
| Exploitant : | Aéroport de Strasbourg-Entzheim|
| Destinations : | Bassin Méditerranéen et Europe de l'Ouest |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de la métropole de Strasbourg |


## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Rennes
* Afficher les prochains départs et arrivées.
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* sites marchands
	* durée de voyage maximale

## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://www.strasbourg.aeroport.fr/FR/Reservez.html
Le moteur de recherche de vol est proposé par Easyvoyage. Ce dernier est un comparateur de voyage, qui propose à ses différents partenaires un moteur de recherche de vol sur plus de 100 sites marchands référencés.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Strasbourg-Rome le 10 Décembre et 8 Janvier 2020 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| limit | :----: | 300 |
| offset | :----: | 0 |
| options | `string` | %7B%22typeTransport%22%3A%22AVION%22%2C%22typeTerminal%22%3A%22WEB%22%7D |
| useClicService | `bool` | true |
| rid | `string`| rid1572173650238rdm937 |


**Destinataire :**
https://era.easyvoyage.com/fr_FR/vols/recherche/1572173649410/results.rjs;jsessionid=9A6063C3E7CB1539024B9AFA77CEB5C0.arabie

**Type du contenu de la réponse :** 
Javascript

**Contenu de la réponse :**
- La requête renvoie un fichier javascript pour chaque site marchand, où les éléments de réponse sont contenus dans un tuple.
- Se référer au document Service_152.rjs pour consulter plus en détail un exemplaire de ces fichiers javascript.

| Nom | Type | Format |
| --- | :----: | :------: |
| index | `string` | 0 |
| note | `int` | 13 |
| group | `string` | 'LH2257LH1848-LH1841LH2256-' |
| ukey | `string` | Se référer au point 1 en dessous du tableau |
| goStops | `int` | 1 |
| returnStops | `int` | 1 |
| stops | `int` | 2 |
| idPartenaire | `int` | 2236 |
| dureeAllerEscalesComprises | `int` | 510 |
| dureeRetourEscalesComprises | `int` | 230 |
| dureeMax | `int` | 510 |
| dureeMin | `int` | 230 |
| prixSansFrais | `float` | 219.0 |
| prixInt | `string` | '219 &euro;' |
| prixValue | `float` | 219.0 |
| id | `string` | 'vol_result_2236_21900_1572173144862_0' |
| dureeAller | `string` | '946711800000' |
| dureeRetour | `string` | '946695000000' |
| dureeTotaleAller | `string` | '510' |
| dureeTotaleRetour | `string` | '230' |
| dateMillisDepartAller | `string` | 1575978600000 |
| dateMillisArriveeAller | `string` | 1576009200000 |
| dateMillisDepartRetour | `string` | 1578468300000 |
| dateMillisArriveeRetour | `string` | 1578482100000 |
| typeTransport | `string` | 'AVION' |
| urlInfos | `string` | Se référer au point 2 en dessous du tableau |
| pubDate | `string` | '27/10/2019 11:45' |

1. ukey =   '2236,21900,AVION;20191210,1250,34641,20191210,1345,35012,0055,LH,2257;20191210,1950,35012,20191210,2120,34714,0090,LH,1848;;20200108,0825,34714,20200108,1000,35012,0095,LH,1841;20200108,1120,35012,20200108,1215,34641,0055,LH,2256;'

2. urlInfos = '/FR/Reservez.html?&site=14471&codeClient=1&cr=1SXBROM20191210,ROMSXB20200108,1,0,0,1&_rechercheId_=1572173649410&searchId=50b7c8ec-dd3b-4c5a-8c2b-87e6b694076a&_widget_=false&_bzz_=false&_partenaire_=2236&_resultId_=vol_result_2236_21900_1572173144862_0&key=9e2d7ecc-2d30-4a15-b4f6-88282f62b221&fluid=_HIMxGIv38uH79-YBZkHoVxQkyuH3aS1stDn3qoYBYIisqSMKViLXPIL3P%2Fn38KmKqIL39EP3PeVX8rL37EV69DHxPwLsqIR3yrM3hIvJP18GqNMo4rTNAR%3DGqNMo4kPFGSKszWMoPS7zvAKszWMoZ6TKAeAo4WV64kMozT%2Fozk%2FozcKszWMoZ6Z3DeAo4WV6auZ3PlL3G1KszWMoZ6MxueAo4WV64DKszWMoZ6MxAeAo4WV64cKszWMoZ6MFueAo4WV64cKszWMoZ6Zx7NA-ZW1rNSKszWMoPSo7DeAo4WHrTrKszWMoPS1xGrls8Elx9s1GqNMo4kP6tuKszWMoP-Ho4kMXzWeXzW%2BGqNMo4kPzHNA-ZW1rNSKszWMoP-Ho4alozkeo4aHrzWMoaekoZkArAeAo4WHrTg1rNkeIzuaGqNMo4rToZWeBzaHozWeBzNMo4co74a%2Br4RKszWMoZ6IIzu%3Do%3DNASDeAo4WV64kMoZWMozW%2Bo4wHrzWMzaweB4-eGqNMo4kP77NA-Za1rNSKszWMoP-Ho4kMo4aMB4aeoZWMoaekoZkAr1eAo4WHryrnGqNMo4rTo9IZB4kVB4SYB4-loPkMxPSZ64Kvo4xvrZxA6toA6tNhr4WAoPkMxueAo4WHryuv3uEV3huHxPuKszWMoPS43PAMxGIRstuAK1eAo4WHryuv3uE%2F69Sns9AKszWMoPS4NaDKszWMoZ6AstACxPD%2FKtDn6PlKszWMoPSDxGrls8Elx9s1KHkQkycAxTSRstNYBZaArVkerVoer4-%2BrZkQk8rHFGS1K81RKHkfkZDz9aI7zvvHo4alozkeo%3De7zvAz9akHo4kMo4aMB%3DMeX4WQo%3DMekYMYKPuRK8ri79-YBYkAotkhxVR1xHAT64rYXzSZr9a%2FBtoHxYv%2BrPNPxZxlr4Whr8aYX%3DIH6GrA3qSI6%3Dkfky6L3DEH6GrA3qSCoZkVr1gHozTMoDgerzKHozKV34DsOGyvHG0gX9p8B9vO9Ejj0Xvv5RbZ6GNN&_urldetails_=https%3A%2F%2Fbooking.promovols.com%2Fsearch%2Ftransport%3Fif%3DSXB%26it%3DROM%26ds%3D2019-12-10%26cl%3DEconomy%26pa%3D1%26pc%3D0%26pi%3D0%26ca%255B0%255D%3DLH%26C%3Deasyvoyage%26de%3D2020-01-08%26O%255B0%255D%3D20191210125000LH2257%26O%255B1%255D%3D20191210195000LH1848%26I%255B0%255D%3D20200108082500LH1841%26I%255B1%255D%3D20200108112000LH2256%26si%3D1bc82384b8493b0cdcd74064665dc5de74053b0a%26utm_source%3DComparateur%26utm_medium%3DCPA%26utm_campaign%3DEasyvoyages&_codeCompagnie_=LH&_prixHF_=219.0'
