
# Service 137 - Aéroport Nice Côte d'Azur

## Informations générales

| | |
| --------- | -----|  
| ID : | 137 |  
| Nom : | Aéroport Nice Côte d'Azur | 
| Site : | [https://www.nice.aeroport.fr/](https://www.nice.aeroport.fr/) |
| Exploitant : | AÉROPORTS DE LA COTE D'AZUR  |
| Destinations : | Bassin Méditerranéen et Europe|
| Description : |  Plateforme de réservation de vol au départ de la métropole de Nice |


## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Nice.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* pertinence
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* la durée cumulée des escale(s)
	* changement d'aéroport durant une escale
	* les options des bagages
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* durée de voyage maximale



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Menu déroulant pour l'aéroport d'arrivée
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://api.optionway.com/rest/air/flights/lowfares.json
Option Way est une agence de voyages, qui propose à ses différents partenaires un moteur de recherche de vol sur plus de 300 compagnies aériennes.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Nantes-Berlin le 4 et 28 Décembre 2019 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| bPartner | `int` | 8678017 |
| bUser | `int` | 173642 |
| bUsername | `string` | YkZaYklzWWI4eCt6Z09nR3UrNHZ0dz09 |
| cabin_class | `string` | Y |
| creationDate | `string` | b2hIMjVOMS9MaFdaeEhKWmxENWxFZz09 |
| from | `string`  | NTE%3BcBER |
| lang | `string` | fr |
| partner | `int` | 4727249 |
| departure_airport | `string` | NTE%3BTXL%2CSXF%2CBER |
| data_groups | `string` | link |
| client_auth | `string` | xBasic |
| api_version | `int` | 4.x |
| authorize_night_stops | `bool` | true%3Btrue |
| compressedFormat | `bool` | false |
| date_out | `date` | 2019-12-04%3B2019-12-28 |
| date_out_etime | `date` | 00%3A00%3B00%3A00 |
| date_out_ltime | `date` | 23%3A59%3B23%3A59 |
| from | `string` | NTE%3BBER |
| from_possible_airport | `string` | NTE%3BTXL%2CSXF%2CBER |
| gds | `string` | 1G |
| luggage_number | `int` | 0 |
| max_connection_time | `int` | 1440%3B1440 |
| max_stop | `int` | 3%3B3 |
| max_travel_time | `int` | 2880%3B2880 |
| partnerLinkId | `int` | 1836687 |
| pax_number | `int` | 1%2C0%2C0 |
| prohibit_multi_airport_connection | `bool` | false |
| return_airport | `string` | TXL%2CSXF%2CBER%3BNTE |
| to | `string` | BER%3BNTE |
| to_possible_airport | `string` | TXL%2CSXF%2CBER%3BNTE |

**Type du contenu de la réponse :** 
XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
- Extrait d'un élément de réponse :
{status: "ok", total: 109, data: [,…], domain: "air", method: "searchFlights"}
- Se référer au document annexe "Service_135.json" pour voir le format de l'élément data dont voici un extrait des 4 premier éléments :

| Nom | Type | Format |
| --- | :----: | :------: |
| bPartner | `int` | 1836687 |
| id | `string` | Zwdtz3IbUxRyPZcOkwnSpoPNHAc_l7jZFIIVcB6agGo |
| fare | `double` | 199.39|
| provider | `string` | 1G |

