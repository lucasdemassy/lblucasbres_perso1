
# Service 157 - Air France

## Informations générales

| | |
| --------- | -----|  
| ID : | 157 |  
| Nom : | Air France | 
| Site : | https://www.airfrance.fr/ |
| Exploitant : | Air France |
| Destinations : | Mondiales |
| Description : |  Site de la première compagnie aérienne française |

## Services proposés

* Possibilité de réserver un vol (avec escale).
* Possibilité de consulter sa réservation.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* date de départ/arrivée
* Possibilité de filtrer les réservations par:
	* un prix maximal
	* le nombre d'escales
	* un créneau horaire
	* compagnies


## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
Carte *Wemap* des destination représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://wwws.airfrance.fr/search/offers

**Type de requête :** 
- POST

**Paramètres envoyés:**
Se référer au fichier Service_157_request pour voir un exemple d'une réservation d'un aller-retour vol Marseille-Paris le 7 et 12 Novembre 2019 en classe économique

**Destinataire :**
https://wwws.airfrance.fr/gql/v1

**Type du contenu de la réponse :** 
XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
Se référer au document Service_157.json pour consulter la réponse du site au format JSON.
