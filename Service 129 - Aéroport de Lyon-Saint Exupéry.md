
# Service 129 - Aéroport de Lyon-Saint Exupéry

## Informations générales

| | |
| --------- | -----|  
| ID : | 129 |  
| Nom : | Aéroport de Lyon-Saint Exupéry | 
| Site : | https://www.lyonaeroports.com/ |
| Exploitant : | Vinci Airports  |
| Destinations : | Europe, Bassin Méditerranéen, et quelques destinations internationales hors U.E. |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Lyon-Saint Exupéry |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lyon-Saint Exupéry.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* pertinence
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* la durée cumulée des escale(s)
	* changement d'aéroport durant une escale
	* les options des bagages
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* durée de voyage maximale
* Calcul d'itinéraire pour accéder à l'aéroport par voiture.
* Réserver un Ouibus ou un train SNCF pour accéder à l'aéroport.



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://api.optionway.com/rest/air/flights/lowfares.json
Option Way est une agence de voyages, qui propose à ses différents partenaires un moteur de recherche de vol sur plus de 300 compagnies aériennes.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Lyon-Berlin le 8 et 23 Février 2020 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| bPartner | `int` | 8678017 |
| bUser | `int` | 173642 |
| bUsername | `string` | a25CeFZBNXJta3lmY3BRUlhRNVhXdz09 |
| cabin_class | `string` | Y |
| creationDate | `string` | RGVqbHErVmkwT1BJS3dzTWRSRGFjQT09 |
| from | `string`  | LYS%3BcBER |
| lang | `string` | fr |
| partner | `int` | 4727249 |
| departure_airport | `string` | LYS%3BTXL%2CSXF%2CBER |
| data_groups | `string` | link |
| client_auth | `string` | xBasic |
| api_version | `int` | 4.x |
| authorize_night_stops | `bool` | true%3Btrue |
| compressedFormat | `bool` | false |
| date_out_etime | `date` | 00%3A00%3B00%3A00 |
| date_out_ltime | `date` | 23%3A59%3B23%3A59 |
| from | `string` | LYS%3BBER |
| from_possible_airport | `string` | LYS%2CXYD%3BTXL%2CSXF%2CBER |
| gds | `string` | 1G |
| luggage_number | `int` | 0 |
| max_connection_time | `int` | 1440%3B1440 |
| max_stop | `int` | 3%3B3 |
| max_travel_time | `int` | 2880%3B2880 |
| partnerLinkId | `int` | 8678017 |
| pax_number | `int` | 1%2C0%2C0 |
| prohibit_multi_airport_connection | `bool` | false |
| return_airport | `string` | TXL%3BLYS%2CXYD |
| to | `string` | TXL%3BLYS |
| to_possible_airport | `string` | TXL%2CSXF%2CBER%3BLYS%2CXYD |



**Type du contenu de la réponse :** 
XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
- Extrait d'un élément de réponse :
{status: "ok", total: 60, data: [,…], domain: "air", method: "searchFlights"}
- Se référer au document annexe "Service_129.json" pour voir le format de l'élément data dont voici un extrait des 5 premier éléments :

| Nom | Type | Format |
| --- | :----: | :------: |
| bPartner | `int` | 8678017 |
| id | `string` | 1UIhWiGS4MmSbsY1yaUJiT7dPrJCZh5CCWtIUCJ9IVk |
| fare | `double` | 143.60|
| provider | `string` | 1G |
| includedFee | `double` | 5.0 |




## Calculer un itinéraire en voiture pour venir à l'aéroport Lyon Saint-Exupéry
**Description :** 
Le site de l'aéroport de Lyon Saint-Exupéry permet à ses utilisateurs de calculer un itinéraire pour venir en voiture jusqu'au terminal d'accueil.

**Type de requête :** 
Redirection vers un autre site

**Destinataire :**
https://www.google.fr/maps

**Paramètres envoyés:**
Le formulaire renvoie vers une adresse Google Maps à partir du texte HTML du champ d'entrée du lieu de départ. Le lieu de destination n'est pas modifiable.

**Type du contenu de la réponse :** 
Nouvelle page HTML

**Contenu de la réponse :**
Page Google Maps calculant l'itinéraire entre le point de départ et l'aéroport Lyon Saint-Exupéry.

