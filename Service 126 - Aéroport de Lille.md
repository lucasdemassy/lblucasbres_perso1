
# Service 126 - Aéroport de Lille

## Informations générales

| | |
| --------- | -----|  
| ID : | 126 |  
| Nom : | Aéroport de Lille | 
| Site : | https://www.lille.aeroport.fr/ |
| Exploitant : | Aéroport de Lille  |
| Destinations : | Bassin Méditerranéen et Europe de l'Ouest |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de la métropole de Lille |


## Services proposés

* Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lille.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* date de départ/arrivée
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire
	* compagnies
	* aéroports/gares


## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier avec les dates possibles mis en surbrillance
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://aeroports-voyages.h24travel.com
h24travel.com est une agence de voyages, qui permet à ses différents partenaires de déléguer la gestion des horaires des différents vols, et des calculs d'itinéraire.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Lille-Marseille le 24 et 27 Octobre 2019 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| i-search-type | `string` | roundTrip |
| cabinClass | `string` | Y |
| origin | `string` | LIL |
| destination | `string` | MRS |
| infants | `int` | 0 |
| returndate | `date` | 27%2f10%2f2019 |
| childs | `int` | 0 |
| searchtype | `string`  | R |
| adults | `int` | 1 |
| godate | `date` | 24%2f10%2f2019 |
| campaign | `string` | home |
| utm_source | `string` | LIL |
| cabinclass | `string` | y |
| utm-medium | `string` | searchengine |
| custom | `string` | affi-LIL |

**Type du contenu de la réponse :** 
- html

## Requête du listing des destinations de l'aéroport de Lille
**Système de requête :** 
Le site de l'aéroport de Lille contient du Javascript et notamment du JQuery. Il est donc interactif et permet d'afficher les prochains départs et toutes les destinations au départ de Lille.

**Type de requête :** 
- POST

**Destinataire :**
[https://www.booking.aeroports-voyages.fr](https://www.booking.aeroports-voyages.fr/)

**Paramètres envoyés:**
Exemple d'une autocomplétion lors de la recherche d'un aéroport de destination en écrivant "Paris" :

| Nom | Type | Format |
| --- | :----: | :------: |
| id_part | `string` | LIL |
| query | `string` | Paris |
| lang: | `string` | fr |
| location_type | `string` | airport |

**Type du contenu de la réponse :** 
- XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
Extrait d'un élément de réponse :

| Nom | Type | Format |
| --- | :----: | :------: |
| country | `string` | France |
| iata | `string` | PAR |
| isCity | `bool` | true |
| name | `string` | Paris |
| id | `int` | 157 |
| country_iso | `string` | FR |

