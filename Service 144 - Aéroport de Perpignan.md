# Service 144 - Aéroport de Perpignan

## Informations générales

| | |
| --------- | -----|  
| ID : | 144 |  
| Nom : | Aéroport de Perpignan | 
| Site : | https://www.aeroport-perpignan.com/ |
| Exploitant : | TRANSDEV - AEROPORT PERPIGNAN  |
| Destinations : | Grande-Bretagne, vols nationaux |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de Perpignan |


## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Perpignan.
* Afficher les prochains départs et arrivées en temps réel.
* Possibilité de trier les réservations par:
	* Le nom de la compagnie
	* prix adulte
	* prix total de la réservation
	* date de départ
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* un créneau horaire
	* compagnies
	* aéroports/gares



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier 
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
 - Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://aeroports-voyages.travelagency.travel/moteur/vol_attente_1.html
- Aéroport Voyages est un portail internet qui facilite la recherche des programmes de vols au départ et à l’arrivée essentiellement des aéroports français, de métropole et des départements d’outre-mer

**Type de requête :** 
- GET

**Paramètres envoyés:**
Exemple d'une réservation d'un vol aller-retour Perpignan - Bruxelles le 6 Novembre et 22 Janvier 2020 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| locked_iata | :----: | PGF |
| type | :----: | R |
| typ | `string` | flight |
| custom | `string` | affi-PGF |
| utm_source | `string` | PGF-SDF |
| utm_medium | `string` | searchengine |
| vil_dep | `string` | PGF |
| vil_ret | `string` | BRU |
| dte_dep | `date` | 06%2F11%2F2019 |
| dte_ret | `date` | 07%2F01%2F2020 |
| adt | `int` | 1 |
| enf | `int` | 0 |
| bb | `int` | 0 |
| all_ret | `bool` | oui |
| cls | `string` | eco |
| idpart | `int`| 947593 |
| navid | `string` | d54401c0-12a3-4137-a7bd-50f24654c427 |


**Type du contenu de la réponse :** 
- La page de recherche des différents vols redirige ensuite automatiquement vers une nouvelle page : https://aeroports-voyages.travelagency.travel/moteur/vol_resultat_2.html
Il est donc impossible de consulter les fichiers reçus par la page de recherche des vols, et donc leur format.
