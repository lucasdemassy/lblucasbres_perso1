
# Service 155 - Aéroport de Toulouse-Blagnac

## Informations générales

| | |
| --------- | -----|  
| ID : | 155 |  
| Nom : | Aéroport de Toulouse | 
| Site : | http://www.toulouse.aeroport.fr/ |
| Exploitant : | Aéroport Toulouse-Blagnac  |
| Destinations : | Bassin Méditerranéen et Europe de l'Ouest |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de la métropole de Toulouse |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lille.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* le moins cher
	* le plus malin
	* le plus court
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* un créneau horaire au départ et à l'arrivée
	* compagnies
	* aéroports/gares
	* sites marchands

## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
Le calcul de la réservation est fourni par Liligo, ce qui signifie que l'aéroport de Toulouse utilise l'API de Liligo pour rechercher les voyages possibles au sein de leurs bases de données fournies par leurs partenaires (aéroports, hôteliers, ...)

**Type de requête :** 
- GET

**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Marseille-Marrakech le 22 Novembre et 5 Décembre 2019 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| apiVersion | `int` | 3 |
| product | `string` | flight |
| searchIndex | `string` | 1|
| count | `int` | 80 |
| c | `int` | 29 |
| r | `double` | 1055348480926999.9 |

**Destinataire :**
http://voyages.toulouse.aeroport.fr/mobileweb_v4/api/results.jsp

**Type du contenu de la réponse :** 
XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
- L'API Liligo renvoie un JSON pour chaque sites marchands. Se référer au document annexe "Service_155.json" pour voir le format de la réponse.
