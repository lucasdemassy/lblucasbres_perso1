# Service 128 - Aéroport de Lorient Bretagne Sud

## Informations générales

| | |
| --------- | -----|  
| ID : | 128 |  
| Nom : | Aéroport de Lorient Bretagne Sud| 
| Site : | [https://www.lorient.aeroport.fr/](https://www.lorient.aeroport.fr/) |
| Exploitant : | CCI Morbihan |
| Destinations : | France |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de la ville de Lorient |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lorient.
* Afficher les prochains départs et arrivées.
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* compagnies
	* aéroports


## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier avec les dates d'aller et retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant)
 - Choix de la classe de voyage (économique, business, ...)


**Web-Cartographie :** 
Carte *leaflet* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : [https://resavol-lorient.visagesdumonde.fr](https://resavol-lorient.visagesdumonde.fr/)
Visages du monde est une agence de voyages pour particuliers et professionnels.

**Type de requête :** 
- GET

**Paramètres envoyés:**
Les paramètres ne sont lisibles par un être humain.

**Type du contenu de la réponse :** 
- HTML

## Requête du listing des destinations de l'aéroport de Lorient Bretagne Sud
**Système de requête :** 
L’auto-complétion d'une destination est effectuée grâce à une recherche sémantique dans un tableau écrit dans un script Javascript lié à la page HTML. Il n'y a pas de communications avec un serveur pour obtenir la liste des aéroports.
