
# Service 143 - Aéroport Pau Pyrénnées

## Informations générales

| | |
| --------- | -----|  
| ID : | 143 |  
| Nom : | Aéroport Pau Pyrénnées | 
| Site : | https://www.pau.aeroport.fr/|
| Exploitant : | Air'py aéroport Pau Pyrénées |
| Destinations : | France principalement |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Pau Pyrénnées |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lille.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* date de départ/arrivée
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire
	* compagnies
	* aéroports/gares



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier 
 - Choix entre un aller simple, ou un aller-retour
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
 - Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://aeroports-voyages.travelagency.travel/moteur/vol_attente_1.html
- Aéroport Voyages est un portail internet qui facilite la recherche des programmes de vols au départ et à l’arrivée essentiellement des aéroports français, de métropole et des départements d’outre-mer

**Type de requête :** 
- GET

**Paramètres envoyés:**
Exemple d'une réservation d'un vol Pau-Paris le 22 Novembre 2019 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| typ | `string` | flight |
| custom | `string` | affi-PUF |
| lng | `string` | fr |
| utm_source | `string` | pau.aeroport.fr |
| utm_medium | `string` | moteur |
| vil_dep | `string` | PUF |
| vil_ret | `string` | PAR |
| dte_dep | `date` | 221119 |
| dte_ret | `date` | 010101 |
| adt | `int` | 1 |
| enf | `int` | 0 |
| bb | `int` | 0 |
| all_ret | `bool` | non |
| cls | `string` | eco |
| idpart | `int`| 947593 |
| navid | `string` | 02e30545-397e-427c-8eb0-337757f3719f |


**Type du contenu de la réponse :** 
- La page de recherche des différents vols redirige ensuite automatiquement vers une nouvelle page HTML : https://aeroports-voyages.travelagency.travel/moteur/vol_resultat_2.html
Il est donc impossible de consulter les fichiers reçus par la page de recherche des vols, et donc leur format.
