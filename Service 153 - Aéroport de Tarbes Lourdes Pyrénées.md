
# Service 153 - Aéroport de Tarbes Lourdes Pyrénées

## Informations générales

| | |
| --------- | -----|  
| ID : | 153 |  
| Nom : | Aéroport de Tarbes Lourdes Pyrénées | 
| Site : | https://www.tlp.aeroport.fr/ |
| Exploitant : | Edeis  |
| Destinations : | Europe |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de la ville de Tarbes |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lille.
* Afficher les prochains départs et arrivées.
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* un créneau horaire
	* compagnies
	* aéroports/gares


## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier avec les dates possibles mis en surbrillance
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
- Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://aeroports-voyages.travelagency.travel/moteur/vol_attente_1.html
- Aéroport Voyages est un portail internet qui facilite la recherche des programmes de vols au départ et à l’arrivée essentiellement des aéroports français, de métropole et des départements d’outre-mer

**Type de requête :** 
- GET

**Paramètres envoyés:**
Exemple d'une réservation d'un vol Tarbes-Lisbonne le 22 Novembre 2019 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| type | `string` | R |
| locked_iata | `string` | LDE |
| typ | `string` | flight |
| custom | `string` | affi-LDE |
| utm_source | `string` | LDE-EDEIS |
| utm_medium | `string` | searchengine |
| vil_dep | `string` | LDE |
| vil_ret | `string` | LIS |
| dte_dep | `date` | 05%2F11%2F2019 |
| dte_ret | `date` | 23%2F11%2F2019 |
| adt | `int` | 1 |
| enf | `int` | 0 |
| bb | `int` | 0 |
| all_ret | `bool` | oui |
| cls | `string` | eco |
| idpart | `int`| 947593 |
| navid | `string` | 893ebc24-306f-4197-a802-8ebf5047d354 |


**Type du contenu de la réponse :** 
- La page de recherche des différents vols redirige ensuite automatiquement vers une nouvelle page : https://aeroports-voyages.travelagency.travel/moteur/vol_resultat_2.html
Il est donc impossible de consulter les fichiers reçus par la page de recherche des vols, et donc leur format.
