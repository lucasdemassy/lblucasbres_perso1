

# Service 145 - Aéroport Rennes Bretagne

## Informations générales

| | |
| --------- | -----|  
| ID : | 148 |  
| Nom : | Aéroport Rennes Bretagne | 
| Site : | https://www.rennes.aeroport.fr/ |
| Exploitant : | Vinci Airports  |
| Destinations directes: | Vols nationaux et Grande-Bretagne principalement|
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Poitiers Biard |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Rennes
* Afficher les prochains départs et arrivées.
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* sites marchands
	* durée de voyage maximale

## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://www.rennes.aeroport.fr/reservation/vol-recherche
Le moteur de recherche de vol est proposé par Easyvoyage. Ce dernier est un comparateur de voyage, qui propose à ses différents partenaires un moteur de recherche de vol sur plus de 100 sites marchands référencés.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Rennes-Barcelone le 17 Décembre et 13 Janvier 2020 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| arrayzed | `bool` | false |
| options | `string` | %7B%22typeTransport%22%3A%22AVION%22%2C%22typeTerminal%22%3A%22WEB%22%7D |
| useClicService | `bool` | true |
| rid | `string`| rid1572169733626rdm808 |


**Destinataire :**
https://era.easyvoyage.com/fr_FR/vols/recherche/1572169732395/resultats.120.rjs;jsessionid=91B589C935A1EB2B1FA044F3291FF4CC.angola

**Type du contenu de la réponse :** 
Javascript

**Contenu de la réponse :**
- La requête renvoie un fichier javascript pour chaque site marchand, où les éléments de réponse sont contenus dans un tuple.
- Se référer au document Service_148.rjs pour consulter plus en détail un exemplaire de ces fichiers javascript.

| Nom | Type | Format |
| --- | :----: | :------: |
| index | `string` | -2147483648 |
| note | `int` | 7 |
| group | `string` | 'AF7639AF1148-AF1349AF7678-' |
| ukey | `string` | Se référer au point 1 en dessous du tableau |
| goStops | `int` | 1 |
| returnStops | `int` | 1 |
| stops | `int` | 2 |
| idPartenaire | `int` | 120 |
| dureeAllerEscalesComprises | `int` | 855 |
| dureeRetourEscalesComprises | `int` | 335 |
| dureeMax | `int` | 855 |
| dureeMin | `int` | 335 |
| prixSansFrais | `float` | 222.99 |
| prixInt | `string` | '223 &euro;' |
| prixValue | `float` | 222.99 |
| id | `string` | 'vol_result_120_22299_1572169602969_0' |
| dureeAller | `string` | '946824600000' |
| dureeRetour | `string` | '946730400000' |
| dureeTotaleAller | `string` | '2390' |
| dureeTotaleRetour | `string` | '820' |
| dateMillisDepartAller | `string` | 1576603500000 |
| dateMillisArriveeAller | `string` | 1576654800000 |
| dateMillisDepartRetour | `string` | 1578914100000 |
| dateMillisArriveeRetour | `string` | 1578934200000 |
| typeTransport | `string` | 'AVION' |
| urlInfos | `string` | Se référer au point 2 en dessous du tableau |
| pubDate | `string` | '27/10/2019 10:46' |

1. ukey =   '120,22299,AVION;20191217,1825,34797,20191217,1935,34406,0855,AF,7639;20191218,0655,34406,20191218,0840,34997,0855,AF,1148;;20200113,1215,34997,20200113,1415,34406,0335,AF,1349;20200113,1645,34406,20200113,1750,34797,0335,AF,7678;'

2. urlInfos = 'https://www.rennes.aeroport.fr/reservation/vol-info?&site=782&codeClient=1&cr=1RNSBCN20191217,BCNRNS20200113,1,0,0,1&_rechercheId_=1572169732395&searchId=d67800fe-2fe9-4b8b-816d-46691993da5a&_widget_=false&_bzz_=false&_partenaire_=120&_resultId_=vol_result_120_22299_1572169602969_0&key=d2ed7573-fc70-4b8f-a3ff-a85887302fcd&fluid=_HIMxGIv38uH79-YBZaHo%3DMYsGIQStuvx91QKHkfk8RvsqcVBYgLshshX8IHxG6L68elX86HXPAVKYEH3huv67EV69DHxPRn38Km6tgO6tuMxGIvsGI1-91HKtEHsDeAo4WV6DIBNAeAo4WHr8DHK81Px9ecFGIM3hIvGqNMo4rT-TrBGqNMo4kP3huvx8EA38SaxG1KszWMoP-erAeAo4WHr8EAstILs9lTz9EmstR669DHGqNMo4rTozkHo4alGqNMo4kPK8uvsGImStDlGqNMo4rTozrKszWMoZ6H6GSAK8lr3PlvFD11xGIKszWMoP-MozkMoZcKszWMoZ6R6quQsqrKszWMoP-eGqNMo4kPxPRn3tSVGqNMo4rToDeAo4WHr81m68DmsqrKszWMoP-MGqNMo4kP-vE%3DNTDBSauaGqNMo4rTSNDz9u6JzDrtN1eAo4WHr8rAKyI138rlGqNMo4rTSuu7GqNMo4kPsGIQGqNMo4rTKPuRK8rioHlT3AeAo4WHryILs9lTsqInKDeAo4WV6qSHs9uKszWMoZ6Z3tDVKv6QF9sisDeAo4WV6D1KszWMoZ68xGI1uq1M6ueAo4WV6DeAo4WHryI1KGu1KhSI6DeAo4WV64oMozTHrVo%2BrZWeBzRKszWMoZ6%2Fx9rKszWMoP-Vo4aloZKVB4xMozT%2BNvrksZWeozxvrZwAr4-hrueAo4WHr8DZ3ySYGqNMo4rT68en6PRvGqNMo4kPsGS%2FGhS1K8AKszWMoPS7z1r%3D-vlKszWMoZ6AstACxPD%2FKtDn6PlKszWMoPSH6GrA3qSVGqNMo4kPsGS%2FGPA16t1A3ueAo4WV6tA1stDCKPuRK8riGqNMo4kPsGS%2FGhrLsGIZ6ueAo4WV6aucNA19zvezS1IKszWMoZ6Y61EVs9IV3huHxPuKszWMoP-%2FX7v%2FXuszoDoMo1c-o4DKszWMoZ6RsGSLxPenxP%2FKszWMoPSvKyu1GqNMo4kPF1eAo4WV64kHoZTloDeAo4WHr8n4GqNMo4rToZkHBzTMGvuuN1eAo4WHr8rMGqNMo4rTo%3DkQkycAxTSRstNYBZaArVkerZTPo4klrZTQk8rHFGS1K81RKHkfkZD7z1r%3D-v%2BHo4alozkerHe%3D-vl7z1oHo4kMo4aeoHMeX4WQo%3DMekYMYKPuRK8ri79-YBYITrZK%2Bo4c867vH68NlXzSYBtk%2FB4aP6%3DvvrZxlozTloPSRr9aYX%3DIH6GrA3qSI6%3Dkfky6L3DEH6GrA3qSCozkMGVkHoZTlGVaArVkerZTPrsFRNIl%2BJQMskNdYb8%3DHYwV6Q7y64j1PhNKA&_urldetails_=https%3A%2F%2Fwww.bravofly.fr%2Fmsr%2Froute%2Fsearching.do%3FdepartureAirport%3DRNS%26arrivalAirport%3DBCN%26outboundDay%3D17%26outboundMonthYear%3D122019%26returnDay%3D13%26returnMonthYear%3D012020%26adults%3D1%26childs%3D0%26infants%3D0%26COBRANDED%3DEASYVOLSFR%26currency%3DEUR%26url%3Dsearch3.do%26roundtrip%3Dtrue%26classFlight%3DY%26fareType%3D%26requestId%3D3019273860198%26mac%3D3019273860198SCHv011646854475%26acntb%3Dflight%26utm_term%3DRNSBCN%26utm_campaign%3Dresults%26utm_medium%3Dmeta_search%26utm_source%3DEASYVOLSFR%26bf_subsource%3D-----WS0S02PP01%26autoclick%3Dtrue%26j%3D222990%26jC%3D222990_EUR%26cp%3D0&_codeCompagnie_=AF&_prixHF_=222.99'
