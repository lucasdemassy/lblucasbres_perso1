
# Service 131 - Aéroport Marseille Provence

## Informations générales

| | |
| --------- | -----|  
| ID : | 131 |  
| Nom : | Aéroport Marseille Provence | 
| Site : | https://www.marseille.aeroport.fr/ |
| Exploitant : | Aéroport Marseille Provence |
| Destinations : | Principalement Europe et Bassin Méditerranéen |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de Marseille |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Marseille.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* le moins cher
	* le plus court
	* le plus malin (compromis entre prix et durée de vol)
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* sites marchands (airfrance.com, lastminute.com, ...)
* Réserver un Ouibus ou un train SNCF pour accéder ou repartir de l'aéroport.



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
Le calcul de la réservation est fourni par Liligo, ce qui signifie que l'aéroport Marseille Provence utilise l'API de Liligo pour rechercher les voyages possibles au sein de leurs bases de données fournies par leurs partenaires (aéroports, hôteliers, ...)

**Type de requête :** 
- GET

**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Marseille-Marrakech le 22 Novembre et 5 Décembre 2019 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| apiVersion | `int` | 3 |
| product | `string` | flight |
| searchIndex | `string` | 1|
| count | `int` | 80 |
| c | `int` | 6 |
| r | `int` | 7623014442561956 |


**Destinataire :**
https://vols.mpaeroport.fr/mobileweb_v4/api/results.jsp

**Type du contenu de la réponse :** 
XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
- L'API Liligo renvoie un JSON pour chaque sites marchands. Se référer au document annexe "Service_131.json" pour voir le format de la réponse.

## Réserver un train ou un bus depuis l'aéroport Marseille Provence
**Description :** 
Le site de l'aéroport de Marseille permet à ses usagers de réserver un bus ou un train pour arriver ou repartir de l'aéroport.

**Type de requête :** 
Redirection vers un autre site

**Destinataire :**
https://www.google.fr/maps

**Paramètres envoyés:**
Le formulaire renvoie vers une adresse Google Maps à partir du texte HTML du champ d'entrée du lieu de départ. Le lieu de destination n'est pas modifiable.

**Type du contenu de la réponse :** 
Nouvelle page HTML

**Contenu de la réponse :**
Page Google Maps calculant l'itinéraire entre le point de départ et l'aéroport Lyon Saint-Exupéry.

