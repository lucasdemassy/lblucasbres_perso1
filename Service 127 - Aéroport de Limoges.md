# Service 127 - Aéroport de Limoges

## Informations générales

| | |
| --------- | -----|  
| ID : | 127 |  
| Nom : | Aéroport de Limoges | 
| Site : | [https://www.aeroportlimoges.com/](https://www.aeroportlimoges.com/) |
| Exploitant : | Aéroport de Limoges  |
| Destinations : | Bassin Méditerranéen et Angleterre |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport de la ville de Limoges |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Limoges.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* date de départ/arrivée
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire
	* compagnies
	* aéroports/gares



## Front-end

**Type d'interface :** 
- Page Internet


**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier avec les dates d'aller et retour
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)
 
**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : [https://aeroports-voyages.h24travel.com](https://aeroports-voyages.h24travel.com/)
h24travel.com est une agence de voyages, qui permet à ses différents partenaires de déléguer la gestion des horaires des différents vols, et des calculs d'itinéraire.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Limoges-Glasgow le 16 et 24 Octobre 2019 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| i-search-type | `string` | roundTrip |
| cabinClass | `string` | Y |
| origin | `string` | LIG |
| destination | `string` | GLA |
| infants | `int` | 0 |
| returndate | `date` | 24%2f10%2f2019 |
| childs | `int` | 0 |
| searchtype | `string`  | R |
| adults | `int` | 1 |
| godate | `date` | 16%2f10%2f2019 |
| campaign | `string` | home |
| utm_source | `string` | LIG |
| cabinclass | `string` | y |
| utm-medium | `string` | searchengine |
| custom | `string` | affi-LIG |

**Type du contenu de la réponse :** 
- HTML

## Requête du listing des destinations de l'aéroport de Limoges
**Système de requête :** 
Le site de l'aéroport de Limoges contient du Javascript et notamment du JQuery. Il est donc intéractif et permet d'afficher les prochains départs et toutes les destinations au départ de Limoges.

**Type de requête :** 
- POST

**Destinataire :**
[https://www.booking.aeroports-voyages.fr](https://www.booking.aeroports-voyages.fr/)

**Paramètres envoyés:**
Exemple de la recherche des tables horaires de départs de Limoges vers Bristol :

| Nom | Type | Format |
| --- | :----: | :------: |
| departure_iata | `string` | LIG |
| arrival_iata | `string` | BRS |
| id_part | `string` | LIG |
| country | `string` | FR |


**Type du contenu de la réponse :** 
XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
Exemple de réponse :

| Nom | Type | Format |
| --- | :----: | :------: |
| departure_iata | `string` | LIG |
| arrival_iata | `string` | BRS |
| departure_id | `ìnt`| 4693 |
| arrival_id | `ìnt` | 211 |
| is_arrival_city | `bool` | false |
| arrival_country | `string` | Grande-Bretagne |
| arrival_name | `string` | Bristol |
| monday | `bool` | true |
| tuesday | `bool` | false |
| wednesday | `bool` | false |
| thursday | `bool` | false |
| friday | `bool` | true |
| saturday | `bool` | false |
| sunday | `bool` | false |
| validity_end | `int[3]` | month, year, day |
| validity_start | `int[3]` | month, year, day |
