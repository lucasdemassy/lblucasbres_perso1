
# Service 139 & 140 - Aéroports de Paris

## Informations générales

| | |
| --------- | -----|  
| ID : | 139 & 140 |  
| Nom : | Aéroports de Paris | 
| Site : | https://www.parisaeroport.fr/passagers |
| Exploitant : | Aéroports de Paris  |
| Destinations : | Internationales|
| Description : |  Plateforme de réservation de vol au départ des aéroports d'Orly et Charles-de-Gaulle |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ des aéroports de Paris.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* date de départ/arrivée
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire
	* compagnies
	* aéroports/gares



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport de départ et d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://parisaeroport-vol.misterfly.com/airresults/
misterfly.com est une agence de voyages, qui dispose d'un moteur de recherche de vols. Elle négocie également des prix avec des compagnies aériennes.

**Type de requête :** 
- GET

**Destinataire :**
https://parisaeroport-vol.misterfly.com/_rest/api/AirFaresSearch/

**Paramètres envoyés:**
Le site de réservation envoie seulement un identifiant de recherche à l'API. Ce dernier n'est pas compréhensible par un humain.

**Type du contenu de la réponse :** 
- XMLHttpRequest (xhr) au format JSON

**Contenu de la réponse :**
Extrait d'un élément de réponse pour un vol Paris-Tokyo le 7 Novembre 2019 (se référer au document *Service_139_140.json* pour le fichier en entier) :

| Nom | Type | Format |
| --- | :----: | :------: |
| id_part | `string` | LIL |
| query | `string` | Paris |
| lang: | `string` | fr |
| location_type | `string` | airport |

Le fichier de réponse est très long, car il contient également tout les aéroports, compagnies, et villes ainsi que leur formatage (par exemple: l'aéroport Orly s'écrit ORY).
Le fichier Service_139_140_extrait.json contient seulement l'information d'un vol.
