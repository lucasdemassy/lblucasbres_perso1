
# Service 145 - Aéroport Poitiers Biard

## Informations générales

| | |
| --------- | -----|  
| ID : | 145 |  
| Nom : | Aéroport de Poitiers | 
| Site : | http://www.poitiers.aeroport.fr/ |
| Exploitant : | Vinci Airports  |
| Destinations directes: | Manchester, Londres, Lyon, Ajaccio |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Poitiers Biard |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Poitiers.
* Afficher les prochains départs et arrivées.
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* sites marchands
	* durée de voyage maximale

## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : http://www.poitiers.aeroport.fr/reservation/vol-recherche
Le moteur de recherche de vol est proposé par Easyvoyage. Ce dernier est un comparateur de voyage, qui propose à ses différents partenaires un moteur de recherche de vol sur plus de 100 sites marchands référencés.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Poitiers-Lyon le 4 Décembre et 9 Janvier 2020 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| arrayzed | `bool` | false |
| options | `string` | %7B%22typeTransport%22%3A%22AVION%22%2C%22typeTerminal%22%3A%22WEB%22%7D |
| useClicService | `bool` | true |
| rid | `string`| rid1572164527534rdm762 |


**Destinataire :**
http://era.easyvoyage.com/fr_FR/vols/recherche/1572164527003/resultats.1208.rjs;jsessionid=BB669294F0680CC6ED483C43F9B9319C.bulgarie

**Type du contenu de la réponse :** 
Javascript

**Contenu de la réponse :**
- La requête renvoie un fichier javascript pour chaque site marchand, où les éléments de réponse sont contenus dans un tuple.
- Se référer au document Service_145.rjs pour consulter plus en détail un exemplaire de ces fichiers javascript.

| Nom | Type | Format |
| --- | :----: | :------: |
| index | `string` | -2147483648 |
| note | `int` | 47 |
| group | `string` | 'CE811--CE812--' |
| ukey | `string` | Se référer au point 1 en dessous du tableau |
| goStops | `int` | 0 |
| returnStops | `int` | 0 |
| stops | `int` | 0 |
| idPartenaire | `int` | 1208 |
| dureeAllerEscalesComprises | `int` | 70 |
| dureeRetourEscalesComprises | `int` | 70 |
| dureeMax | `int` | 70 |
| dureeMin | `int` | 70 |
| prixSansFrais | `float` | 196.45 |
| prixInt | `string` | '196 &euro;' |
| prixValue | `float` | 196.45 |
| id | `string` | 'vol_result_1208_19645_1572163836233_3' |
| dureeAller | `string` | '946685400000' |
| dureeRetour | `string` | '946685400000' |
| dureeTotaleAller | `string` | '70' |
| dureeTotaleRetour | `string` | '70' |
| dateMillisDepartAller | `string` | 1575439200000 |
| dateMillisArriveeAller | `string` | 1575443400000 |
| dateMillisDepartRetour | `string` | 1578555900000 |
| dateMillisArriveeRetour | `string` | 1578560100000 |
| typeTransport | `string` | 'AVION' |
| urlInfos | `string` | Se référer au point 2 en dessous du tableau |
| pubDate | `string` | '27/10/2019 09:10' |

1. ukey =   '1208,19645,AVION;20191204,0700,35736,20191204,0810,34452,0070,CE,811;;;20200109,0845,34452,20200109,0955,35736,0070,CE,812;;'

2. urlInfos = 'http://easyvoyage.liligo.fr/partner/fredirect.jsp;jsessionid=C620E0F9CEDCA3B19BA819839C18C115.node14?deeplinkId=gIDzit270Yo8DAwQEAAB&partnerId=201115&label=FR-785-4-12168-1-web&easy_theme=babylon/aff/poitiersaeroportfr&easy_partner=le+site+de+l%27a%C3%A9roport+de+Poitiers+et+Easyvoyage&easy_client=12168&lang=fr_FR&easy_extra=p:1208/ck:1PISLYS20191204,LYSPIS20200109,1,0,0,1/searchId:d9dede95-ead1-4a87-812b-806eaae89ca0'


