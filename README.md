To convert a Markdown File:

1. Go to Visual Studio Code
2. Download the extension Markdown PDF
3. Open the Markdown file in Visual Studio Code
4. Press `F1` or `Ctrl+Shift+P`
5. Type `export` and select below
   * `markdown-pdf: Export (pdf)`


This extension is provided by yzane, check out his Github : https://github.com/yzane/vscode-markdown-pdf