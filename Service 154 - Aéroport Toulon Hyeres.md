
# Service 154 - Aéroport Toulon Hyeres

## Informations générales

| | |
| --------- | -----|  
| ID : | 154 |  
| Nom : | Aéroport de Toulon Hyères | 
| Site : | https://www.toulon-hyeres.aeroport.fr/) |
| Exploitant : | Vinci Airports |
| Destinations : | France et Belgique principalement |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Toulon Hyères |


## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Toulon Hyères
* Afficher les prochains départs et arrivées.
* Possibilité de filtrer les réservations par:
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire du jour de départ et d'arrivée
	* compagnies
	* aéroports/gares
	* sites marchands
	* durée de voyage maximale

## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, ou un aller-retour
 - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
Carte *Google Maps* des destinations représentées sous forme de *marker*.


## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://www.toulon-hyeres.aeroport.fr/reservation/vol-recherche
Le moteur de recherche de vol est proposé par Easyvoyage. Ce dernier est un comparateur de voyage, qui propose à ses différents partenaires un moteur de recherche de vol sur plus de 100 sites marchands référencés.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Hyères-Bruxelles le 20 Novembre et 22 Décembre 2019 en classe économique pour un adulte:

| Nom | Type | Format |
| --- | :----: | :------: |
| arrayzed | `bool` | false |
| options | `string` | %7B%22typeTransport%22%3A%22AVION%22%2C%22typeTerminal%22%3A%22WEB%22%7D |
| useClicService | `bool` | true |
| rid | `string`| rid1572194319353rdm833 |



**Destinataire :**
https://era.easyvoyage.com/fr_FR/vols/recherche/1572194317269/resultats.1220.rjs;jsessionid=6C4A724BA39A68355D69882C768BCC24.finlande

**Type du contenu de la réponse :** 
Javascript

**Contenu de la réponse :**
- La requête renvoie un fichier javascript pour chaque site marchand, où les éléments de réponse sont contenus dans un tuple.
- Se référer au document Service_152.rjs pour consulter plus en détail un exemplaire de ces fichiers javascript.

| Nom | Type | Format |
| --- | :----: | :------: |
| index | `string` | -2147483648 |
| note | `int` | 45 |
| group | `string` | 'FR6005--FR6318--' |
| ukey | `string` | Se référer au point 1 en dessous du tableau |
| goStops | `int` | 0 |
| returnStops | `int` | 0 |
| stops | `int` | 0 |
| idPartenaire | `int` | 1220 |
| dureeAllerEscalesComprises | `int` | 95 |
| dureeRetourEscalesComprises | `int` | 100 |
| dureeMax | `int` | 100 |
| dureeMin | `int` | 95 |
| prixSansFrais | `float` | 89.14 |
| prixInt | `string` | '89 &euro;' |
| prixValue | `float` | 89.14 |
| id | `string` | 'vol_result_1220_8914_1572194239644_12' |
| dureeAller | `string` | '946686900000' |
| dureeRetour | `string` | '946687200000' |
| dureeTotaleAller | `string` | '95' |
| dureeTotaleRetour | `string` | '100' |
| dateMillisDepartAller | `string` | 1574261400000 |
| dateMillisArriveeAller | `string` | 1574267100000 |
| dateMillisDepartRetour | `string` | 1576993500000 |
| dateMillisArriveeRetour | `string` | 1576999500000 |
| typeTransport | `string` | 'AVION' |
| urlInfos | `string` | Se référer au point 2 en dessous du tableau |
| pubDate | `string` | '27/10/2019 17:37' |

1. ukey =   '1220,8914,AVION;20191120,1550,34462,20191120,1725,34746,0095,FR,6005;;;20191222,0645,34887,20191222,0825,34462,0100,FR,6318;;'

2. urlInfos = 'http://easyvoyage.liligo.fr/partner/fredirect.jsp;jsessionid=878B2E08667B00BAFCE547140F63C56F.node17?deeplinkId=gIDT0qy2xYs8DAzoB-gHAAE&partnerId=201115&label=FR-788-4-12171-1-web&easy_theme=babylon/aff/toulon-hyeresaeroportfr&easy_partner=le+site+de+l%27a%C3%A9roport+de+Toulon+et+Easyvoyage&easy_client=12171&lang=fr_FR&easy_extra=p:1220/ck:1TLNBRU20191120,BRUTLN20191222,1,0,0,1/searchId:e17a5d33-362d-492e-a980-419754de1721'
