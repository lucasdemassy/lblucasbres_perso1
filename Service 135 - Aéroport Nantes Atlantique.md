
# Service 135 - Aéroport Nantes Atlantique

## Informations générales

| | |
| --------- | -----|  
| ID : | 135 |  
| Nom : | Aéroport Nantes Atlantique | 
| Site : | [https://www.nantes.aeroport.fr/fr](https://www.nantes.aeroport.fr/fr) |
| Exploitant : | Vinci Airports  |
| Destinations : | Europe, pays du Maghreb et du Moyen-Orient, |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Nantes Atlantique |



## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lille.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* date de départ/arrivée
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* les options des bagages
	* un créneau horaire
	* compagnies
	* aéroports/gares



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, un aller-retour
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)

**Web-Cartographie :** 
*Non présente*

## Requête et Back-End d'une réservation

**Système de requête :** 
 Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : [https://aeroports-voyages.h24travel.com](https://aeroports-voyages.h24travel.com/)
h24travel.com est une agence de voyages, qui permet à ses différents partenaires de déléguer la gestion des horaires des différents vols, et des calculs d'itinéraire.

**Type de requête :** 
- GET


**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Nice-Manchester le 26 Décembre et 9 Janvier 2020 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| cabinClass | `string` | y | 
| origin | `string` | nce_nce | 
| destination | `string` | MAN_MAN | 
| returndate | `date` | 09%2f01%2f2020 | 
| searchtype | `string`  | R |
| adults | `int` | 1 | * 
| godate | `date` | 26%2f12%2f2019 | 
| sens | `string`| depart |
| resaToAirport | `string` | International%2c+Manchester+(MAN) |
| resaFromAirport | `string` | Nice |
| searchtype | `string` | roundtrip |
| formResaVolButton= | `string` | Valider |

**Type du contenu de la réponse :** 
- html
