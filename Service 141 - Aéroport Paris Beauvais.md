


# Service 141 - Aéroport Paris Beauvais

## Informations générales

| | |
| --------- | -----|  
| ID : | 141 |  
| Nom : | Aéroport Paris Beauvais | 
| Site : | [https://www.aeroportparisbeauvais.com/passagers/](https://www.aeroportparisbeauvais.com/passagers/) |
| Exploitant : | Chambre de Commerce et d’Industrie de l’Oise |
| Destinations : | Bassin Méditerranéen et Europe de l'Est |
| Description : |  Plateforme de réservation de vol au départ de l'aéroport Paris Beauvais |

## Services proposés

 * Possibilité de réserver un vol (avec escale).
* Lister toutes les destinations possibles au départ de l'aéroport de Lille.
* Afficher les prochains départs et arrivées.
* Possibilité de trier les réservations par:
	* prix
	* durée
	* heure de départ
* Possibilité de filtrer les réservations par:
	* une fourchette de prix
	* le nombre d'escales
	* la durée des escales
	* le lieu d'escale
	* les options des bagages
	* un créneau horaire
	* compagnies
	* aéroports/gares



## Front-end

**Type d'interface :** 
- Page Internet

**Intéraction avec l'utilisateur :** Formulaire
 - Champ d'entrée texte pour l'aéroport d'arrivée
    * Autocomplétion
 - Calendrier
 - Choix entre un aller simple, un aller-retour, ou un voyage avec des destinations différentes
  - Nombre de personnes voyageant et leur catégorie d'âge (adulte, enfant, bébé)
 - Choix de la classe de voyage (économique, business, ...)

**Web-Cartographie :** 
*Non présente*


## Requête et Back-End d'une réservation

**Système de requête :** 
Le calcul de la réservation redirige vers une nouvelle page à l'adresse suivante : https://aeroportparisbeauvais-vol.resatravel.com/search
resatravel.com est un service de réservation de voyages.

**Type de requête :** 
- GET

**Paramètres envoyés:**
Exemple d'une réservation d'un aller-retour vol Beauvais-Amsterdam le 13 Novembre et 17 Décembre 2019 en classe économique :

| Nom | Type | Format |
| --- | :----: | :------: |
| from | `string` | Paris%20-%20Beauvais |
| to | `string` | Amsterdam%20Schiphol |
| begin_date | `date` | 20191113 |
| end_date | `date` | 20191217 |
| adult | `int` | 1 |
| child |`int`| 0 |
| infant | `int` | 0 |


**Type du contenu de la réponse :** 
- La page de recherche des différents vols redirige ensuite automatiquement vers une nouvelle page :https://aeroportparisbeauvais-vol.resatravel.com/results
Il est donc impossible de consulter les fichiers reçus, et donc leur format.
